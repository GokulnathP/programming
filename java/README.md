# Java

## Execution

1. Verify java version.
2. Compile java program
3. Run java program

```cmd
>> java --version
>> javac filename.java
>> java classname
```

## Basic Hello World program

```java
public class MyClass {
    public static void main(String[] args){
        System.out.println("Hello World");
    }
}
```

Class name must be match with filename for public class. Every program must have main function. Every statement must end with semicolon(;).

## Comments

There are two types of commends in java

1. Single line commend - //
2. Multi line commend - /\* \*/
3. Documentation commend - /\*\* \*/

```java
// This is single line commend
/* This multi line commend */
/** This is documentation command */
```

## Variables

Variable is name given to a memeory location.
Identifier is used to identify specific entity.
All variables are identifiers. But all identifiers not a variable. Function name, class name, varaible name and so on all are identifiers.

```java
// datatype variablename;
int num;// declaration

// datatype variablename = value;
int age = 20; // declaration with initialization

// variablename = value;
age = 21; // Assign value to varaiblename
```

## Data types

-   Primitive data types
    -   byte
    -   short
    -   int
    -   long
    -   float
    -   double
    -   char
    -   boolean
-   Non-Primitive data types
    -   String
    -   Array
    -   Class

Primitve data type specifies size and type of variable value and doesn't have additional methods.
Non primitive data types also called as reference types and start with upper character. double number have 'd' at end (1.023d). float number have 'f' at end (1.023f).

## Type casting

Converting one data type into other type.

-   Widening casting (Automatically) <br>
    byte -> short -> char -> int -> long -> float -> double
    ```java
    int myInt = 5; // 5
    double myDouble = myInt; // 5.0
    ```
-   Narrowing casting (Manually) <br>
    double -> float -> long -> int -> char -> short -> byte
    ```java
    double myDouble = 5d; // 5.0
    int myInt = (int) myDouble; // 5
    ```

## Operators

-   Arithmetic operator (+, -, \*, /, %, ++, --)
-   Assignment operator (=, +=, -=, \*=, /=, %=, &=, |=, ^=, >>=, <<=)
-   Comparison operator (==, !=, <, >, <=, >=)
-   Logical operator (&&, ||, !)
-   Bitwise operator (&, |, !, ^, >>, <<, >>>)

## String

```java
String str = "test", str1 = "est";

int len = str.length();
int index = str.indexOf(str1);

String upperStr = str.toUpperCase();
String lowerStr = str.toLowerCase();

String output = str.concat(str1);
String output2 = "test" + "est";

//                        start, end(optional)
String sub = str.substring(0,1);
char c = str.charAt(0);
```

## Math

```java
int x = 2, y = -3;
Math.max(x,y); // 2
Math.min(x,y); // -3

Math.abs(y); // 3
Math.sqrt(4); // 2
Math.random(); // 0(include) to 1(exclude)

Math.round(2.5); // 3
Math.ceil(2.5); // 3.0
Math.floor(2.5) // 2.0
```

## Condition

### IF... ELSE...

```java
if(condition1){
    statement1;
} else if(condition2) {
    statement2;
} else {
    statement3;
}
```

### Ternary Operator

```java
// variable = condition ? statement1 : statement2;

int a = 2 > 5 ? 2 + 5 : 5 - 2;
```

### Switch

```java
switch(expression){
    case x:
        statement1;
        break;
    case y:
        statement2;
        break;
    ...
    default:
        statementN;
}
```

## Loop

### While

```java
initialization;
while(condition){
    statement;
}

(e.g);

int i = 0;
while(i<10){
    System.out.println(i);
    i++;
}
```

### Do... While...

```java
initialization;
do{
    statement;
}while(condition);

(e.g);

int i = 0;
do{
    System.out.println(i);
    i++;
}while(i<10);
```

### For

```java
for(initialization; condition; increment/decrement){
    statement;
}

(e.g);

for(int i=0; i<10; i++){
    System.out.println(i);
}
```

### For... Each...

```java
array;
for(type variable : array){
    statement;
}

(e.g);

int[] number = {1,2,3,4,5};
for(int num : number){
    System.out.println(num);
}
```

## Break & Continue

-   break is used to get out of loop or switch statement.
-   continue is used to continue with next iteration instead of doing remaining of current iteration.

## Array

```java
int[] numbers = {1,2,3,4,5};

for(int i = 0; i<numbers.length; i++){
    num[i] -= 1;
}

for(int number : numbers){
    System.out.println(number);
}

int[][] multi_dimensional = {{1,2,3}, {4,5,6}};

for(int i = 0; i<multi_dimensional.length; i++){
    for(int j=0; j<multi_dimensional[i].length; j++){
        System.out.println(multi_dimensional[i][j]);
    }
}
```

## Methods

Block of code runs only when it is called. Main purpose of methods is reusablity of code.

**Syntax**

```java
return_type method_name(parameters){
    statements;
}

method_name(arguments); // Method calling
```

**e.g**

```java
public class MyClass {
    static void myMethod(){
        System.out.println("Hello");
    }
    static int add(int x, int y){
        return x+y;
    }
    public static void main(String[] args){
        myMethod();
        myMethod();
        System.out.println(add(2,3));
    }
}
```

## Method overloading

Multiple methods can have same name with different parameters.

```java
public class MyClass {
    public static int add(int x, int y){
        return x + y;
    }

    public static double add(double x, double y){
        return x + y;
    }

    public static void main(String[] args){
        System.out.println(add(1,2)); // 3
        System.out.println(add(1.2, 3.4)); // 4.6
    }
}
```

## Scope

Variables are only accessible inside the region they are created.

### Method scope

Variables created inside the method are access only inside the method.

### Block scope

Block level scope means variable created inside { } only accessible inside the { }.

## Recursion

Recursion is technique of making a function call itself.

```java
public class MyClass {
    public static int factorial(int n){
        if(n <= 1){
            return 1;
        }
        return n * factorial(n-1);
    }

    public static void main(String[] args){
        System.out.println(factorial(5));
    }
}
```

## OOP

We use Object Oriented Programming because of,
* It is faster and easier to execute
* Reusablitity of code DRY(Don't Repeat Yourself) 
* Clear structure of program

**Attribute** - Variables inside the class is known as attribute. <br/>
**Methods** - Function inside the class is known as methods. <br />
**Constructor** - A function name as class name run when instance of object is created. Used to initialize values and it also take parameters.

```java
public class MyClass{ // Object
	int x; // Attribute

	public MyClass(int y){ // Constructor
		x = y;
	}

	public void myMethod(){ // Method
		System.out.println("From myMethod");
	}

	public static void main(String[] args){
		MyClass myObj = new MyClass(10); // Instance or object of class
		System.out.println(myObj.x); // Accessing attribute of object
		myObj.myMethod(); // Accessing method of object
	} 
}
```

## Modifier
We divide modifiers into two groups
* Access Modifier
* Non-Access Modifier

### Access Modifiers:
For **classes**
* public - any class can access
* default - only class inside package can access

For **attribute** and **method**
* public - any calss can access
* private - only inside class we can access
* protected - The class and inherited class can access
* default - only class inside package can access

### Non Access Modifiers:
For **classes**
* final - Other class cannot inherited the class
* abstract - Class cannot used to create objects

For **attribute** and **methods**
* final - Cannot override the value or modified it
* static - Can access using class without instanciate it
* abstract - Only used in abstract class. It won't have body
* transient - Skipped those while serializing the object
* synchronized - only access by one thread at time
* volatile - always read from main memory not cached

## Encapsulation
It used to make sensitive data hidden from user by accessing them with get and set methods.

```java
public class MyClass{
	private String name;
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public static void main(String[] args){
		MyClass myObj = new MyClass();
		// myObj.name; // throw error
		myObj.setName("Tester");
		System.out.println(myObj.getName());
	}
}
```

## Package and API

Packages are used to group related classes. Packages are divided into two categories,
* Built-in package
* User-defined package

### Build-in Packages

We can import total package or only import specific class of the package

```java
import java.util.*; // Import total package
import java.util.Scanner; // Import only Scanner class of package

public class MyClass {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		System.out.println(name);
	}
}
```

### User-defined packages

Package which created by us. 

#### The structure of package is like below

- root
	- mypack
		- MyPackageClass.java
		
#### To create package

```java
package mypack;
class MyPackageClass {
	public static void main(String[] args){
		System.out.println("This is my package");
	}
}
```

#### To compile and run package

1. Compile the class
```bash
javac MyPackageClass.java
```
2. Complie the package
```bash
javac -d . MyPackageClass.java
```
3. Run the class
```bash
java mypack.MyPackageClass
```

## Inheritance

We can inherit attributes and methods of one class to another. It categories into two,
* Subclass(child)
* Superclass(parent)

To inherit we use **extends**.

```java
class Parent{
	protected String name = "Testing";

	public void greet(){
		System.out.println("Hello everyone!");
	}
}

class Child extends Parent{
	public static void main(String[] args){
		Child child1 = new Child();
		System.out.println(child1.name);
		child1.greet();
	}
} 
```

## Polymorphism

Polymorphism means "many forms". It is used to override inherited attributes and methods from parent class

```java
class Animel{
	public void sound(){
		System.out.println("I am animel");
	}
}

class Pig extends Animel {
	public void sound(){
		System.out.println("I am pig");
	}
}

class Dog extends Animel {
	public void sound(){
		System.out.println("I am dog");
	}
}

class MyClass{
	public static void main(String[] args){
		Animel myAnimel = new Animel();
		Animel myPig = new Pig();
		Animel myDog = new Dog();

		myAnimel.sound(); // I am animel
		myPig.sound(); // I am pig
		myDog.sound(); // I am dog 
	}
}
```

Always child class methods and attributes have preference than parent class.

## Inner Class

Used to structure the program in readable way.

```java
class OuterClass {
	int x = 4;
	class InnerClass {
		int y = x*3;
	}
}

class MyClass {
	public static void main(String[] args){
		OuterClass outer = new OuterClass();
		OuterClass.InnerClass inner = outer.new InnerClass();
		System.out.println(outer.x + " " + inner.y);
	}
}
```

## Abstract and Interface

Abstract class cannot used to create objects.  They only inherit by other class to provide more security. They can have normal methods and abstract methods. Abstract methods cannot have body they must implement by child class.

```java
abstract class Animel {
	abstract public void sleep();
	public void sound(){
		System.out.println("I will make sound");
	}
}

class Pig extends Animel {
	public void sleep(){
		System.out.println("I am sleeping");
	}
}

public class MyClass{
	public static void main(String[] args){
		Pig pig = new Pig();
		pig.sound();
		pig.sleep();
	}
}
```

Interface are abastract class. Only provide abstract methods. They can inherit using **implements** keyword. We can implement more than one interface by seperating using **,(comma)**. But more than one class cannot inherit.

```java
interface class Animel {
	public void sound();
}

interface class Live{
	public void sleep();
}

class Pig implements Animel, Live{
	public void sound(){
		System.out.println("I will make sound");
	}
	public void sleep(){
		System.out.println("I am sleeping");
	}
}

public class MyClass{
	public static viod main(String[] args){
		Pig pig = new Pig();
		pig.sound();
		pig.sleep();
	}
}
```

## Enums

An enum is special class to represent group of constants.

```java
public class Enumeration{
	enum Level {LOW, MEDIUM, HARD};
	public static void main(String[] args){
		for(Level value: Level.values()){
			System.out.println(value);
		}
	}
}
```

## Input

We use java Scanner class for fetching input.

```java
import java.util.Scanner;

public class MyClass{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		int age = input.nextInt();
		float mark = input.nextFloat();
		boolean passed = input.nextBoolean();
	}
}
```

## Date Time

We use java.time package for getting and formating date and time in java.<br>
It provides following,
* LocalDate
* LocalTime
* LocalDateTime
* DateTimeFormatter

```java
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTime {
	public static void main(String[] args){
		LocalDate date = LocalDate.now();
		LocalTime time = LocalTime.now();
		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(date + " " + time + " " + dateTime);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
		String formatedDate = dateTime.format(formatter);
		System.out.println(formatedDate);
	}
}
```

## ArrayList and Collections

java.util package provide ArrayList class by using it we can extends it length and add remove edit elements. It also provide Collections class by using it we can sort an array. ArrayList require array of objects so we cannot use normal int, boolean, char, double instead we can use Integer, Boolean, Character, Double equivalent classes.

```java
import java.util.ArrayList;
import java.util.Collections;

public class MyClass {
	public static void main(String[] args){
		ArrayList<Integer> ages = new ArrayList();

		ages.add(10);
		ages.add(34);
		ages.add(23);
		ages.add(12);
		System.out.println(ages);
		
		ages.set(3, 30);
		System.out.println(ages.get(3));
		ages.remove(0);
		
		Collections.sort(ages);
		for(Integer age: ages){
			System.out.println(age);
		}
		
		ages.clear();
		System.out.println(ages);
	}
}
```

## LinkedList

LinkedList and ArrayList have same methods but the implementation is different for each. LinkedList has following methods extra,

* getFirst
* getLast
* addFirst
* addLast
* removeFirst
* removeLast

## HashMap

LinkedList and ArrayList are 0 based index. For HashMap we can set index it canbe any type of object.

```java
import java.util.HashMap;

public class HashMapClass {
	public static void main(String[] args){
		HashMap<String, String> person = new HashMap<String, String>();
		person.put("Test", "ABC");
		System.out.println(person.get("Test"));
		System.out.println(person) + " " + person.keySet() + " " + person.values());
		person.remove("Test");
		person.clear();
		System.out.println(person);
	}
}
```

## HashSet

HashSet can only have a value single time cannot have duplicates. It doesn't have index based access. 

```java
import java.util.HashSet;

public class HashSetClass {
	public static void main(String[] args){
		HashSet<String> names = new HashSet<String>();
		names.add("Test 1");
		names.add("Test 2");
		names.add("Test 1");
		System.out.println(names + " " + names.contains("Test 2"));
		names.remove("Test 2");
		for(String name: names){
			System.out.println(name);
		}
	}
}
```

## Iterator

Iterator is object used to loop through objects.

```java
import java.util.ArrayList;
import java.util.Iterator;

public class IteratorClass {
	public static void main(String[] args){
		ArrayList<String> names = new ArrayList<String>();
		names.add("Hello");
		names.add("Hi");
		names.add("Hey");
		
		Iterator it  = names.iterator();
		while(it.hasNext()){
			String name = it.next();
			if(name == "Hi"){
				it.remove();
			}
		}
		System.out.println(names);
	}
}
```

## Wrapper class

Every primite data type has it own wrapper class in java. For int -> Integer, char -> Character , double -> Double and so on.

They provide extra features like toString(), length() and so on.

```java
public class MyClass{
	public static void main(String[] args){
		Integer myInt = 100;
		String myString = myInt.toString();
		System.out.println(myString.length());
	}
}
```

## Exceptions

When a run time error occurs we can use exception to nicely handle it.

```java
public class MyClass{
	public static void main(String[] args){
		try{ // block of code may have error in runtime
			throw new Exception("Custome error"); // create a custom error
		}catch(Exception e){ // block of code when error occurs
			System.out.println(e);
		}finally { // block of code run after try or catch
			System.out.println("After try...catch...");
		}
	}
}
```

## Regular Expressions

Regular expressions provide to find a pattern in string. We use java.util.regex package which provide following classes.
- Pattern - used to define pattern
- Matcher - used to search for pattern

```java
java.util.regex.Pattern;
java.util.regex.Matcher;

public class MyClass{
	public static void main(String[] args){
		Pattern pattern = Pattern.compile("w3schools", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher("Visit W3schools!");
		boolean matchFound = matcher.find();
		if(matchFound){
			System.out.println("Match is found");
		} else {
			System.out.println("Match is not found");
		}
	}
}
```

### Patterns:
- [abc] - accept any one in the list
- [^abc] - accept anything else in the list
- [0-9] - accept anything between 0 to 9

### Meta characters
- | - or opperator cat|dog
- . - one instance of any character
- ^ - matches in the beginning 
- $ - matches in the ending
- \d - find digit
- \s - find space

### Quantifier

- n+ - any string atleast one n
- n* - any string zero or more occurance of n
- n? - any string zero or one occurance of n
- n{x} - any string contains sequence of x n's
- n{x,y} - any string contains sequence of x to y n's

## Threads

There are two ways to create threads. First one extending Thread class and second one is implementing Runnable interface.

1. Threads class

```java
public class MyClass extends Thread {
	public static void main(String[] args){
		MyClass thread = new MyClass();
		thread.start();
		System.out.println("I am running from main class");
	}

	public void run(){
		System.out.println("I am running from thread class");
	}
}
```

2. Runnable interface

```java
public class MyClass implements Runnable {
	public static void main(String[] args){
		MyClass myObj = new MyClass();
		Thread thread = new Thread(myObj);
		thread.start();
		System.out.println("I am running from main class");
	}

	public void run() {
		System.out.println("I am running from thread class");
	}
}
```

## Lambda

syntax,

```
(parameter) -> expression
(parameter1, parameter2) -> expression
(parameter1, parameter2) -> { code block }
```

(e.g)

```java
import java.util.ArrayList;

public class MyClass {
	public static void main(String[] args){
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		numbers.add(12);
		numbers.add(10);
		numbers.add(9);

		numbers.forEach(n -> {System.out.println(n)});
	}
}

```

## File

1. Create a file

```java
import java.io.File;

public class MyClass {
	public static void main(String[] args){
		try {
			File myObj = new File("myFile.txt");
			if(myObj.createNewFile()){
				System.out.println("File created successfully");
			} else {
				System.out.println("File already exist");
			}
		} catch(Exception e){
			System.out.println(e);
		}
	}
}
```

2. Write into file

```java
import java.io.FileWriter;

public class MyClass {
	public static void main(String[] args){
		try {
			FileWriter myObj = new FileWriter("myFile.txt");
			myObj.write("Something");
			myObj.close();
		} catch(Exception e){
			System.out.println(e);
		}
	}
}
```

3. Read from file

```java
import java.io.File;
import java.util.Scanner;

public class MyClass{
	public static void main(String[] args){
		try {
			File myObj = new File("myFile.txt");
			Scanner fileReader = new Scanner(myObj);

			while(fileReader.hasNextLine()){
				System.out.println(myReader.nextLine());
			}

			myReader.close();
		} catch(Exception e){
			System.out.println(e);
		}
	}
}
```

4. Delete a file

```java
import java.io.File;

public class MyClass {
	public static void main(String[] args){
		try {
			File myObj = new File("myFile.txt");
			myObj.delete();
		} catch(Exception e){
			System.out.println(e);
		}
	}
}
```

