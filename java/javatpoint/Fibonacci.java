import java.util.*;

public class Fibonacci {
	public static void withoutRecursion(int n){
		int a = 0, b = 1, c;
		for(int i = 0; i<n; i++){
			System.out.println(a);
			c = a+b;
			a = b;
			b = c;
		}
	}
	static int a = 0, b = 1, c;
	public static void withRecursion(int n){
		if(n > 0){
			System.out.println(a);
			c = a + b;
			a = b;
			b = c;
			withRecursion(n-1);
		}
	}
	public static void withDefaultRecursion(int n1, int n2, int n){
		if(n > 0){
			System.out.println(n1);
			int n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			withDefaultRecursion(n1, n2, n-1);
		}
	}
	public static void main(String[] args){
		int n;
		System.out.print("Enter n value: ");
		Scanner input = new Scanner(System.in);
		n = input.nextInt();
		withDefaultRecursion(0, 1, n);
	}
}
