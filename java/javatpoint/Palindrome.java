import java.util.*;

public class Palindrome{
	
	public static boolean isPalindrome(String word){
		String reverse = "";
		for(int i=word.length()-1; i>=0; i--){
			reverse = reverse + word.charAt(i);
		}
		if(word.equals(reverse)){
			return true;
		}
		return false;
	}

	public static boolean isPalindrome(int n){
		int temp = n, reverse = 0;
		while(temp > 0){
			reverse = reverse*10 + temp%10;
			temp /= 10;
		}
		if(reverse == n){
			return true;
		}
		return false;
	}

	public static void main(String[] args){
		int n;
		String word;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter word value: ");
		word = input.nextLine();
		
		System.out.print("Enter n value: ");
		n = input.nextInt();

		if(isPalindrome(n)){
			System.out.println(n + " is Palindrome number");
		} else {
			System.out.println(n + " is not a Palindrome number");
		}

		if(isPalindrome(word)){
			System.out.println(word + " is Palindrome number");
		} else {
			System.out.println(word + " is not a Palindrome number");
		}
	}
}
