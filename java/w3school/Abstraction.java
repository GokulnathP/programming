abstract class Animel {
	public abstract void sound();
	public void sleep(){
		System.out.println("I am sleeping");
	}
}

class Pig extends Animel {
	public void sound(){
		System.out.println("I am Pig");
	}
}

public class Abstraction{
	public static void main(String[] args){
		Animel pig = new Pig();
		pig.sound();
		pig.sleep();
	}
}
