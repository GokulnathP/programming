public class ArrayClass {
    public static void main(String[] args) {
        // single dimensional array
        int[] numbers = { 1, 2, 3, 4, 5 };

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + "\t");
        }
        System.out.println("");

        // multi dimensional array
        int[][] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8 } };

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println("");
        }
    }
}