public class Condition {
    public static void main(String[] args) {
        // if...else
        int x = 2, y = 5;
        if (x < y) {
            System.out.println("x is less than y");
        } else if (x == y) {
            System.out.println("x is equal to y");
        } else {
            System.out.println("x is greater than y");
        }

        // switch...case
        int a = 3;
        switch (a) {
            case 0:
                System.out.println("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Three");
                break;
            default:
                System.out.println("Something else");
        }
    }
}