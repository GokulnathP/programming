import java.time.LocalTime;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTime{
	public static void main(String[] args){
		LocalDate date = LocalDate.now();
		LocalTime time = LocalTime.now();
		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(date + " " + time + " " + dateTime);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
		String formatedDate = dateTime.format(formatter);
		System.out.println(formatedDate);
	}
}
