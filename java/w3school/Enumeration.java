public class Enumeration{
	enum Level { LOW, MEDIUM, HARD };
	public static void main(String[] args){
		for(Level value: Level.values()){
			System.out.println(value);
		}
	}
}
