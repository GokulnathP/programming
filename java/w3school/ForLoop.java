public class ForLoop {
    public static void main(String[] args) {
        // break & continue
        for (int i = 1;; i++) {
            if (i % 5 == 0 && i % 3 == 0) {
                break;
            }
            if (i % 3 == 0) {
                continue;
            }
            System.out.print(i + "\t");
        }
        System.out.println("");
    }
}