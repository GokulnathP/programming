public class FunctionClass {
    public static void greet() {
        System.out.println("Welcome to learn Java");
    }

    public static int add(int x, int y) {
        return x + y;
    }

    public static double add(double x, double y) {
        return x + y;
    }

    public static int sum_of_n_numbers(int n) {
        if (n <= 0) {
            return 0;
        }
        return n + sum_of_n_numbers(n - 1);
    }

    public static void main(String[] args) {
        greet();
        greet();
        System.out.println(add(10, 2));
        System.out.println(add(10.2, 2));
        System.out.println(sum_of_n_numbers(4));
    }
}