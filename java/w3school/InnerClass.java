class OuterClass1 {
	int x = 1;
	class InnerClass1 {
		int y = 2*x;
	}
}

class OuterClass2 {
	int x = 2;
	private class InnerClass2 {
		int y = 2*x;
	}

	public int getY(){
		InnerClass2 inner2 = new InnerClass2();
		return inner2.y;
	}
}

class OuterClass3 {
	int x = 3;
	static class InnerClass3 {
		OuterClass3 outer3 = new OuterClass3();
		int y = outer3.x * 2;
	}
}

class InnerClass{
	public static void main(String[] args){
		OuterClass1 outer1 = new OuterClass1();
		OuterClass1.InnerClass1 inner1 = outer1.new InnerClass1();
		System.out.println(outer1.x + " " + inner1.y);

		OuterClass2 outer2 = new OuterClass2();
		System.out.println(outer2.x + " " + outer2.getY());

		OuterClass3 outer3 = new OuterClass3();
		OuterClass3.InnerClass3 inner3 = new OuterClass3.InnerClass3();
		System.out.println(outer3.x + " " + inner3.y);
	}
}
