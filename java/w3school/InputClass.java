import java.util.Scanner;

public class InputClass{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		int age = input.nextInt();
		float mark = input.nextFloat();
		boolean passed = input.nextBoolean();
		System.out.println(name + " is " + age + " year old scored " + mark + " in exam and passed = " + passed );
	}
}
