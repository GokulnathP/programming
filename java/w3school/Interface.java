abstract class Animel {
	abstract public void sound();
	public void sleep() {
		System.out.println("I am sleeping");
	}
}

interface Living {
	public void live();
}

interface Earth {
	public void plant();
}

class Pig extends Animel implements Living, Earth {
	public void sound(){
		System.out.println("I am Animel");
	}

	public void plant(){
		System.out.println("I am from earth");
	}

	public void live(){
		System.out.println("I am living");
	}
}

public class Interface{
	public static void main(String[] args){
		Pig pig = new Pig();
		pig.sound();
		pig.sleep();
		pig.plant();
		pig.live();
	}
}
