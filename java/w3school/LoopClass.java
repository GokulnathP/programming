public class LoopClass {
    public static void main(String[] args) {
        // while
        int i = 0;
        while (i < 10) {
            System.out.print(i + "\t");
            i++;
        }
        System.out.println("");

        // do while
        do {
            System.out.print(i + "\t");
            i--;
        } while (i > 0);
        System.out.println("");
        ;

        // for
        for (i = 0; i < 10; i++) {
            System.out.print(i + "\t");
        }
        System.out.println("");

        // for each
        int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        for (int number : numbers) {
            System.out.print(number + "\t");
        }
        System.out.println("");
    }
}