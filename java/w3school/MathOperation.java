public class MathOperation {
    public static void main(String[] args) {
        int x = 2, y = -3;
        System.out.println(Math.max(x, y)); // 2
        System.out.println(Math.min(x, y)); // -3

        System.out.println(Math.abs(y)); // 3
        System.out.println(Math.sqrt(4)); // 2.0
        System.out.println(Math.random()); // 0 to 1

        System.out.println(Math.round(2.5)); // 3
        System.out.println(Math.ceil(2.5)); // 3.0
        System.out.println(Math.floor(2.5)); // 2.0
    }
}