// Class name must be match with filename
public class MyClass {
    // Every program must have main method
    public static void main(String[] args) {
        /*
         * The // is used for single line comment. The /* *\/ is used for multi line
         * comment. The \ is escape character
         */
        /*
         * Variables: 1. int 2. char 3. float 4. boolean 5. String
         */
        int a = 1;
        float b = 2.5f;
        char c = 'a';
        String d = "Gokulnath ";
        boolean e = false;
        System.out.println("Hello World! " + d + c); // Print Hello World! in a line
        System.out.println(a + " " + b + ' ' + e);

        // Widening type casting (automatically)
        int myInt = 18;
        double myDouble = myInt;
        System.out.println(myDouble);

        // Narrowing type casting (manually)
        double myDouble2 = 18.023d;
        int myInt2 = (int) myDouble2;
        System.out.println(myInt2);

    }
}