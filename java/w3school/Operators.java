public class Operators {
    public static void main(String[] args) {
        int num1 = 1, num2 = 2;
        // Arithmetic operators
        System.out.println(num1 + num2); // 3
        System.out.println(num1 - num2); // -1
        System.out.println(num1 * num2); // 2
        System.out.println(num1 / num2); // 0
        System.out.println(num1 % num2); // 1
        System.out.println(num1++ + num2); // 3
        System.out.println(num1 + --num2); // 3
        System.out.println(num1 + " " + num2); // 2 1
        // Asignment opertor
        int num3 = num2; // 1
        num3 += num1; // 3
        num3 -= num1; // 1
        num3 *= num1; // 2
        num3 /= num1; // 1
        System.out.println(num3); // 1
        // Comparison operator
        System.out.println(num2 == num3); // true
        System.out.println(num2 != num3); // false
        System.out.println(num1 < num3); // false
        System.out.println(num1 > num3); // true
        System.out.println(num2 >= num3); // true
        System.out.println(num2 <= num3); // true
        // Logical operator
        System.out.println(num1 == num3 && num2 == num3); // false
        System.out.println(num1 == num3 || num2 == num3); // true
        System.out.println(!(num1 == num3 || num2 == num3)); // false
        // Bitwise operator
        System.out.println(num1 & (num1 + num2)); // 2
        System.out.println(num1 | num2); // 3
        System.out.println(num1 ^ (num2 + num1)); // 1
        System.out.println(~num1); // -3
        System.out.println(9 >> 1); // 4
        System.out.println(9 << 1); // 18
        System.out.println(9 >>> 1); // 4
    }
}