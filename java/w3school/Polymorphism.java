class Animel {
	public void sound(){
		System.out.println("I am animel");
	}
}

class Pig extends Animel {
	public void sound() {
		System.out.println("I am pig");
	}
}

class Dog extends Animel {
	public void sound(){
		System.out.println("I am dog");
	}
}

public class Polymorphism {
	public static void main(String[] args){
		Animel animel = new Animel();
		Pig pig = new Pig();
		Animel dog = new Dog();

		animel.sound();
		pig.sound();
		dog.sound();
	}
}
