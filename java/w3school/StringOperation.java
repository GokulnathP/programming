public class StringOperation {
    public static void main(String[] args) {
        String firstname = "Tester", lastname = "Testing";

        System.out.println(firstname.substring(0, 2));

        System.out.println(firstname.length()); // 5
        System.out.println(firstname.indexOf('o')); // 1

        System.out.println(firstname.toUpperCase()); // TESTER
        System.out.println(lastname.toLowerCase()); // testing

        System.out.println(firstname.concat(lastname)); // TesterTesting
        System.out.println(firstname + lastname); // TesterTesting
        System.out.println("2" + 10); // 210

        System.out.println("Testing is \"good\""); // Testing is "good"
        System.out.print("Hello\t");
        System.out.print("World \n"); // Hello World
        System.out.println("He\rllo\rWorld"); // World
        System.out.println("Hello\bWorld"); // HellWorld
        System.out.println("Hello\fWorld"); /*
                                             * HelloWorld World
                                             */
    }
}