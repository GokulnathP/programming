public class MyClass {
	public static void main(String[] args){
		OtherClass obj1 = new OtherClass(5, 7);
		OtherClass obj2 = new OtherClass(9, 4);
		obj1.x = 15;
		// obj1.y = 6; // throws error
		System.out.println(obj1.x); // 15
		System.out.println(obj2.x); // 10
		System.out.println(obj1.y); // 5

		OtherClass.greet(); // Hello User
		System.out.println(obj1.add(2,3)); // 5
		obj2.greet(); // Hello User
		System.out.println(obj2.add(2.3,4)); // 6.3

		System.out.println(obj1.sub()); // 2
		System.out.println(obj2.sub()); // 5
	}
}

