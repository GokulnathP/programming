public class OtherClass {
	int x = 10;
	final int y = 5;

	int c, d;

	static void greet() {
		System.out.println("Hello User");
	}

	public OtherClass(int c, int d){
		this.c = c;
		this.d = d;
	}

	public int sub(){
		return Math.abs(this.c - this.d);
	}

	public int add(int a, int b){
		return a+b;
	}

	public double add(double a, double b){
		return a+b;
	}
}
