import java.util.ArrayList;
import java.util.Collections;

public class ArrayListClass{
	public static void main(String[] args){
		ArrayList<String> names = new ArrayList();
		names.add("Test 1");
		names.add("Test 2");
		names.add("Test 3");

		System.out.println(names);

		System.out.println(names.get(1));
		names.set(1, "Test 2.1");
		System.out.println(names.get(1));
		names.remove(1);
		System.out.println(names.get(1));

		Collections.sort(names);
		for(int i = 0; i< names.size(); i++){
			System.out.println(names.get(i));
		}

		names.clear();
		System.out.println(names);
	}
}
