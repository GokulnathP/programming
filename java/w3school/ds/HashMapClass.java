import java.util.HashMap;

public class HashMapClass {
	public static void main(String[] args){
		HashMap<String, String> person = new HashMap<String, String>();

		person.put("Test 1", "ABC");
		person.put("Test 2", "XYZ");
		person.put("Test 3", "PQR");

		System.out.println(person);
		System.out.println(person.get("Test 1"));

		for(String name: person.keySet()){
			System.out.println(name);
		}

		for(String college: person.values()){
			System.out.println(college);
		}

		person.remove("Test 2");
		System.out.println(person + " " + person.size());

		person.clear();
		System.out.println(person);
	}
}
