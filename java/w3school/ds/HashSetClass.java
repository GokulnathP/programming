import java.util.HashSet;

public class HashSetClass {
	public static void main(String[] args){
		HashSet<String> roleno = new HashSet<String>();

		roleno.add("006");
		roleno.add("015");
		roleno.add("127");
		roleno.add("015");
		roleno.add("135");

		System.out.println(roleno + " " + roleno.contains("127"));
		roleno.remove("127");
		System.out.println(roleno + " " + roleno.size() + " " + roleno.contains("127"));
		
		for(String role: roleno){
			System.out.println(role);
		}

		roleno.clear();
		System.out.println(roleno);
	}
}
