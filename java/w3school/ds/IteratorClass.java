import java.util.ArrayList;
import java.util.Iterator;

public class IteratorClass {
	public static void main(String[] args){
		ArrayList<String> names = new ArrayList<String>();
		names.add("test 1");
		names.add("test 2");
		names.add("test 3");

		Iterator<String> it = names.iterator();

		while(it.hasNext()){
			String name = it.next();
			if(name == "test 2"){
				it.remove();
			}
		}
		System.out.println(names);
	}
}
