import java.util.LinkedList;

public class LinkedListClass {
	public static void main(String[] args){
		LinkedList<String> names = new LinkedList<String>();

		names.add("Test 1");
		names.add("Test 2");
		names.addFirst("Test 0");
		names.addLast("Test 3");

		System.out.println(names);
		System.out.println(names.getFirst());
		System.out.println(names.getLast());
		System.out.println(names.get(1));

		names.set(1, "Test 2.1");
		names.remove(2);
		names.removeFirst();
		names.removeLast();
		System.out.println(names);

		names.clear();
		System.out.println(names);
	}
}
